const { Router } = require('express');

const controller = require('../controller/dictionary');

const router = Router();

router.route('/')
  .get((req, res, next) => controller
    .getTerms()
    .then(terms => res.status(200).send(terms))
    .catch(() => next())
  );

router.route('/')
  .post((req, res, next) => controller
    .createTerm(req.body)
    .then(term => res.status(200).send(term))
    .catch(() => next())
  );

router.route('/:termId')
  .delete((req, res, next) => controller
    .deleteTerm(req.params.termId)
    .then(term =>
      res.status(200).send(term?'Item deleted':'Item not found or already deleted')
    )
    .catch(() => next())
  );

router.route('/:termId')
  .put((req, res, next) => controller
    .updateTerm(req.params.termId, req.body)
    .then(term =>res.status(200).send(term))
    .catch(() => next())
  );


module.exports = router;
