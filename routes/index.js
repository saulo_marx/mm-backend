const { Router } = require('express');
const dictionariesRouter = require('./dictionariesRouter');

const router = Router();

router.use('/dictionaries', dictionariesRouter);

module.exports = router;
