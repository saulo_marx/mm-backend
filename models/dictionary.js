module.exports = function (sequelize, DataTypes) {
  return sequelize
    .define(
      'Dictionary', {
        title: {
          type: DataTypes.STRING,
        },
        description: {
          type: DataTypes.STRING,
        },
      },
      {
        freezeTableName: true,
        paranoid: true,
      },
    );
};
