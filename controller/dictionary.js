const Database = require('../config/database');

const { models: { Dictionary } } = Database();

module.exports = {
  getTerms() {
    return Dictionary.findAll();
  },
  createTerm(term) {
    return Dictionary.create(term);
  },
  deleteTerm(termId) {
    return Dictionary
    .destroy({ where: { id: termId } });
  },
  updateTerm(termId, object) {
    return Dictionary.findById(termId).then(term=>term.update(object));
  },
};
