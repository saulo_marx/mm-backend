module.exports = {

  up: function(queryInterface, Sequelize) {
    return queryInterface.sequelize.transaction({ autocommit: true }, t => {
      const dictionaries = [{
        createdAt: Sequelize.fn('now'),
        updatedAt: Sequelize.fn('now'),
        description:'Teste 01',
        title:'Teste 01',
      },{
        createdAt: Sequelize.fn('now'),
        updatedAt: Sequelize.fn('now'),
        description:'Teste 02',
        title:'Teste 02',
      },{
        createdAt: Sequelize.fn('now'),
        updatedAt: Sequelize.fn('now'),
        description:'Teste 03',
        title:'Teste 03',
      }];

      return queryInterface.bulkInsert('Dictionary', dictionaries);
    });
  },

  down(queryInterface, Sequelize) {
    return queryInterface.sequelize.transaction({ autocommit: true}, t => {
      return queryInterface.bulkDelete('Dictionary');
    });
  }
};
