module.exports = {
  up(queryInterface, Sequelize) {
    return queryInterface.sequelize.transaction({ autocommit: true }, t => {
      return queryInterface
        .createTable('Dictionary',
          {
            id: {
              type: Sequelize.INTEGER,
              primaryKey: true,
              autoIncrement: true
            },
            createdAt: { type: Sequelize.DATE },
            updatedAt: { type: Sequelize.DATE },
            deletedAt: { type: Sequelize.DATE },
            title: { type: Sequelize.STRING },
            description: { type: Sequelize.STRING },
          }
        );
    });
  },

  down(queryInterface, Sequelize) {
    return queryInterface.sequelize.transaction({ autocommit: true }, t => {
      return queryInterface.dropTable('Dictionary');
    });
  }
};
