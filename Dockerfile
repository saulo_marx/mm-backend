FROM node:alpine

WORKDIR /src

COPY . /src
RUN npm config set unsafe-perm true
RUN npm install -g sequelize-cli
RUN npm install --silent
RUN npm install -g pm2

CMD ["npm", "run", "db:seed"]

CMD ["npm", "run", "serve"]
