const supertest = require('supertest');
const { expect } = require('chai');

const app = require('../../app');

const request = supertest(app);

describe('Products api', function () {
  it('GET /products returns an array of products', function () {
    request
      .get('/products')
      .expect(res => expect(res.body).to.be.object('array'));
  });
});
